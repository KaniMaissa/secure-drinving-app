package com.example.pc.securedriving;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener ,SurfaceHolder.Callback {
    ImageView call,direction,message,help,menu,arret;
    private int PICK_CONTACT=123;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    CustomDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main);
        //menu=findViewById(R.id.menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        surfaceView = (SurfaceView) findViewById(R.id.surface_camera);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        dialog = new CustomDialog(this).createDialog(this);
       // setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Log.i(null , "Video starting");


    /*    surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        try {
            startRecording();
        } catch (IOException e) {
            String message = e.getMessage();
            Log.i(null, "Problem Start"+message);
            mrec.release();
        }*/
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);

        View navView =  navigationView.getHeaderView(0);
        //ImageView img = (ImageView)navView.findViewById(R.id.imageView);

        //img.setImageResource(R.drawable.logo);

     /*   img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

        });
*/
        navigationView.setNavigationItemSelectedListener(this);
        call=findViewById(R.id.call);
        message=findViewById(R.id.message);
        direction=findViewById(R.id.direction);
        help=findViewById(R.id.help);
        arret=findViewById(R.id.end);

        direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "http://maps.google.com/maps";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                callIntent.setData(Uri.parse("tel:" + 29131038));
*/
                Intent intent =   new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);



            }
        });
        arret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils. exitApp(MainActivity.this);
            }
        });
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Accueil fragment = new Accueil() ;
        fragmentTransaction.add(R.id.containerfragment,fragment);
        fragmentTransaction.commit();
         }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        // View v=()v.findViewById(R.layout.nav_header_drawer);
        if (id == R.id.nav_settings) {
            // Handle the camera action

          /*  Intent intent = new Intent(MainActivity.this,CustomDialog.class);
            startActivity(intent);*/
            showDialog(this,"hhh");



        } else if (id == R.id.nav_help) {

            Intent intent = new Intent(MainActivity.this,MainActivity.class);
            startActivity(intent);
            finish();


        } else if (id == R.id.nav_about) {

            Intent intent = new Intent(MainActivity.this,MainActivity.class);
            startActivity(intent);
            finish();


        } else if (id == R.id.nav_help) {

            Intent intent = new Intent(MainActivity.this,MainActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_close) {
            Utils. exitApp(MainActivity.this);

        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

public void showDialog (Activity activity, String string)
{
    final Dialog dialog = new Dialog(activity);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setCancelable(true);
    dialog.setContentView(R.layout.dialogsettings);
    dialog.show();
}
}
